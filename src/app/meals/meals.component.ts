import { Component, OnDestroy, OnInit } from '@angular/core';
import { MealsService } from '../shared/meals.service';
import { Meal } from '../shared/meal.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-meals',
  templateUrl: './meals.component.html',
  styleUrls: ['./meals.component.css']
})
export class MealsComponent implements OnInit, OnDestroy {
  meals!: Meal[];
  mealsChangeSubscription!: Subscription;
  mealsFetchingSubscription!: Subscription;
  mealsRemovingSubscription!: Subscription;
  isFetching = false;
  isRemoving = false;

  constructor(private mealsService: MealsService) { }

  ngOnInit(): void {
    this.meals = this.mealsService.getMeals();
    this.mealsChangeSubscription = this.mealsService.mealsChange.subscribe((meals: Meal[]) => {
      this.meals = meals;
    });
    this.mealsFetchingSubscription = this.mealsService.mealsFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.mealsRemovingSubscription = this.mealsService.mealsRemoving.subscribe((isRemoving: boolean) => {
      this.isRemoving = isRemoving;
    });
    this.mealsService.fetchMeals();
  }

  onRemove(id: string, index: number) {
    this.mealsService.removeMeal(id).subscribe(() => {
      this.mealsService.fetchMeals();
    });
    this.meals[index].isRemoving = true;
  }

  ngOnDestroy(): void {
    this.mealsChangeSubscription.unsubscribe();
    this.mealsFetchingSubscription.unsubscribe();
    this.mealsRemovingSubscription.unsubscribe();
  }
}
