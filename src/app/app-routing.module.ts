import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MealFormComponent } from './meal-form/meal-form.component';
import { MealResolverService } from './shared/meal-resolver.service';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'meals/new', component: MealFormComponent},
  {
    path: 'meals/edit/:id', component: MealFormComponent,
    resolve: {
      meal: MealResolverService
    }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
