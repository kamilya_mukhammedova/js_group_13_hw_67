import { Component, OnDestroy, OnInit } from '@angular/core';
import { Meal } from '../shared/meal.model';
import { MealsService } from '../shared/meals.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-total-calories',
  templateUrl: './total-calories.component.html',
  styleUrls: ['./total-calories.component.css']
})
export class TotalCaloriesComponent implements OnInit, OnDestroy {
  meals!: Meal[];
  mealsChangeSubscription!: Subscription;
  mealsFetchingSubscription!: Subscription;
  isFetching = false;
  totalCalories = 0;

  constructor(private mealsService: MealsService) { }

  ngOnInit(): void {
    this.meals = this.mealsService.getMeals();
    this.mealsChangeSubscription = this.mealsService.mealsChange.subscribe((meals: Meal[]) => {
      this.meals = meals;
    });
    this.mealsFetchingSubscription = this.mealsService.mealsFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.mealsService.fetchMeals();
  }

  onTotalCalories() {
    this.totalCalories = 0;
    const today = new Date();
    const day = today.getDate();
    const month = today.getMonth() + 1;
    const year = today.getFullYear();
    const currentDate = year + '-' + month + '-' + day;
    this.meals.forEach(meal => {
        if(meal.date === currentDate) {
          this.totalCalories += meal.calories;
        }
    });
    return this.totalCalories;
  }

  ngOnDestroy(): void {
    this.mealsChangeSubscription.unsubscribe();
    this.mealsFetchingSubscription.unsubscribe();
  }
}
