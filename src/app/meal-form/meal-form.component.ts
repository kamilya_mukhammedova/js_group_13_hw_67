import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MealsService } from '../shared/meals.service';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';
import { Meal } from '../shared/meal.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-meal-form',
  templateUrl: './meal-form.component.html',
  styleUrls: ['./meal-form.component.css']
})
export class MealFormComponent implements OnInit, OnDestroy {
  @ViewChild('f') mealForm!: NgForm;
  isEdit = false;
  isUploading = false;
  editedId = '';
  mealsUploadingSubscription!: Subscription;

  constructor(
    private mealsService: MealsService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.mealsUploadingSubscription = this.mealsService.mealUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });
    this.route.data.subscribe(data => {
      const meal = <Meal | null>data.meal;
      if (meal) {
        this.isEdit = true;
        this.editedId = meal.id;
        this.setFormValue({
          date: meal.date,
          mealTime: meal.mealTime,
          description: meal.description,
          calories: meal.calories
        });
      } else {
        this.isEdit = false;
        this.editedId = '';
        const today = new Date();
        const day = today.getDate();
        const month = today.getMonth() + 1;
        const year = today.getFullYear();
        this.setFormValue({
          date: year + '-' + month + '-' + day,
          mealTime: '',
          description: '',
          calories: ''
        });
      }
    });
  }

  setFormValue(value: {[key: string]: any}) {
    setTimeout(() => {
      this.mealForm.form.setValue(value);
    });
  }

  saveMeal() {
    const id = this.editedId || Math.random().toString();
    const meal = new Meal(
      id,
      this.mealForm.value.date,
      this.mealForm.value.mealTime,
      this.mealForm.value.description,
      this.mealForm.value.calories,
    );
    if(this.isEdit) {
      this.mealsService.editMeal(meal).subscribe(() => {
        this.mealsService.fetchMeals();
      });
    } else {
      if(this.mealForm.value.mealTime === '' || this.mealForm.value.description === '' || this.mealForm.value.calories === '') {
        alert('Please fill all the fields.');
      } else {
        this.mealsService.addMeal(meal).subscribe(() => {
          this.mealsService.fetchMeals();
          void this.router.navigate(['/']);
        });
      }
    }
  }

  ngOnDestroy(): void {
    this.mealsUploadingSubscription.unsubscribe();
  }
}
