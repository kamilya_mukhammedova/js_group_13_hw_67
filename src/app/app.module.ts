import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { MealsComponent } from './meals/meals.component';
import { MealFormComponent } from './meal-form/meal-form.component';
import { TotalCaloriesComponent } from './total-calories/total-calories.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { MealsService } from './shared/meals.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    MealsComponent,
    MealFormComponent,
    TotalCaloriesComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [MealsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
