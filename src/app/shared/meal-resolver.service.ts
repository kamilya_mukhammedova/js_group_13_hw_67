import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Meal } from './meal.model';
import { EMPTY, Observable, of } from 'rxjs';
import { MealsService } from './meals.service';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MealResolverService implements Resolve<Meal>{

  constructor(private mealsService: MealsService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Meal> | Observable<never> {
    const mealId = <string>route.params['id'];
    return this.mealsService.fetchOneMeal(mealId).pipe(mergeMap(meal => {
      if(meal) {
        return of(meal);
      }
      void this.router.navigate(['/']);
      return EMPTY;
    }));
  }
}
