export class Meal {
  isRemoving = false;
constructor(
  public id: string,
  public date: string,
  public mealTime: string,
  public description: string,
  public calories: number,
) {}
}
