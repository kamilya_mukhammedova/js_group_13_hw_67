import { Meal } from './meal.model';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class MealsService {
  mealsChange = new Subject<Meal[]>();
  mealsFetching = new Subject<boolean>();
  mealsRemoving = new Subject<boolean>();
  mealUploading = new Subject<boolean>();

  private mealsArray: Meal[] = [];
  constructor(private http: HttpClient) {}

  getMeals() {
    return this.mealsArray.slice();
  }

  fetchMeals() {
    this.mealsFetching.next(true);
    this.http.get<{[id: string]: Meal}>('https://kamilya-61357-default-rtdb.firebaseio.com/meals.json')
      .pipe(map(result => {
        if(result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const mealData = result[id];
          return new Meal(id, mealData.date,mealData.mealTime, mealData.description, mealData.calories);
        });
      }))
      .subscribe(meals => {
        meals.sort((a,b) => {
          return this.dateToNumber(a.date) - this.dateToNumber(b.date);
        });
        this.mealsArray = meals.reverse();
        this.mealsChange.next(this.mealsArray.slice());
        this.mealsFetching.next(false);
      }, () => {
        this.mealsFetching.next(false);
      });
  }

  dateToNumber(date: string) {
    const dateSplit = date.split('-');
    return Number(dateSplit[0] + dateSplit[1] + dateSplit[2]);
  }

  fetchOneMeal(id: string) {
    return this.http.get<Meal>(`https://kamilya-61357-default-rtdb.firebaseio.com/meals/${id}.json`)
      .pipe(map(result => {
        if(!result) {
          return null;
        }
        return new Meal(id, result.date, result.mealTime, result.description, result.calories);
      }));
  }

  addMeal(meal: Meal) {
    const body = {
      date: meal.date,
      mealTime: meal.mealTime,
      description: meal.description,
      calories: meal.calories
    }
    this.mealUploading.next(true);
    return this.http.post('https://kamilya-61357-default-rtdb.firebaseio.com/meals.json', body)
      .pipe(tap(() => {
        this.mealUploading.next(false);
      }, () => {
        this.mealUploading.next(false);
      }));
  }

  editMeal(meal: Meal) {
    this.mealUploading.next(true);
    const body = {
      date: meal.date,
      mealTime: meal.mealTime,
      description: meal.description,
      calories: meal.calories
    };
    return this.http.put(`https://kamilya-61357-default-rtdb.firebaseio.com/meals/${meal.id}.json`, body)
      .pipe(tap(() => {
        this.mealUploading.next(false);
      }, () => {
        this.mealUploading.next(false);
      }));
  }

  removeMeal(id: string) {
    this.mealsRemoving.next(true);
    return this.http.delete(`https://kamilya-61357-default-rtdb.firebaseio.com/meals/${id}.json`)
      .pipe(tap(() => {
        this.mealsRemoving.next(false);
      }, () => {
        this.mealsRemoving.next(false);
      }));
  }
}
